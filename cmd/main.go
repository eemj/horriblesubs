package main

import (
	"log"

	hs "gitlab.com/eemj/horriblesubs"
)

func main() {
	shows, err := hs.GetLatest()

	if err != nil {
		log.Fatal(err)
	}

	for _, show := range *shows {
		log.Printf(
			"%s Episode %s released %v with %+v available qualities\n",
			show.Title,
			show.Episode,
			show.Released,
			show.Qualities,
		)
	}
}
