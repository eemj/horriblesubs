package horriblesubs

import (
	"net/http"
	"sort"

	"golang.org/x/net/html"
)

type Show struct {
	Title, Link string
}

func GetShows() (shows *[]Show, err error) {
	shows = new([]Show)

	req, err := http.NewRequest(http.MethodGet, "https://horriblesubs.info/shows", nil)

	if err != nil {
		return
	}

	setUserAgent(req)

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	z := html.NewTokenizer(res.Body)

	for {
		tt := z.Next()

		if tt == html.ErrorToken {
			break
		} else if tt != html.StartTagToken {
			continue
		}

		if t := z.Token(); t.Data == "div" {
			// ensure that the div class that we found contains a `.ind-show` selector
			indShow := false
			for _, attr := range t.Attr {
				if attr.Key == "class" && attr.Val == "ind-show" {
					z.Next()
					t = z.Token()
					indShow = true
					break
				}
			}

			if !indShow {
				continue
			}

			show := new(Show)

			for _, attr := range t.Attr {
				switch attr.Key {
				case "href":
					(*show).Link = attr.Val[7:] // strip the `/shows/` prefix
				case "title":
					(*show).Title = attr.Val
				}
			}

			(*shows) = append((*shows), *show)
		}
	}

	// ensure that the results are sorted
	sort.Slice(*shows, func(i, j int) bool {
		return (*shows)[i].Link < (*shows)[j].Link
	})

	return
}
