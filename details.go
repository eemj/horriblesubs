package horriblesubs

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"

	"golang.org/x/net/html"
)

var ErrNoShowID = errors.New("Cannot get show ID")

type (
	Torrent struct {
		Quality string
		Magnet  string
	}

	Episode struct {
		Episode  string
		XDCC     bool
		Torrents []Torrent
	}

	Details struct {
		Episodes    []Episode
		Title       string
		Link        string
		Image       string
		Description string
		ID          int
	}
)

func getEpisodes(ctx context.Context, anime string, page, showID int) (episodes *[]Episode, err error) {
	req, err := http.NewRequest(
		http.MethodGet,
		fmt.Sprintf(
			"https://horriblesubs.info/api.php?method=getshows&type=show&showid=%s&nextid=%s",
			strconv.Itoa(showID),
			strconv.Itoa(page),
		),
		nil,
	)
	req.Header.Set("Referrer", getAnimePage(anime))
	req.WithContext(ctx)
	setUserAgent(req)

	if err != nil {
		return
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	episodes = new([]Episode)
	z := html.NewTokenizer(res.Body)

	var episode *Episode

	for {
		tt := z.Next()

		if tt == html.ErrorToken {
			if episode != nil {
				(*episodes) = append((*episodes), (*episode))
			}
			break
		} else if tt != html.StartTagToken {
			continue
		}

		if t := z.Token(); t.Data == "strong" {
			if episode != nil {
				(*episodes) = append((*episodes), (*episode))
			}
			episode = new(Episode)
			z.Next()
			episodeName := z.Token().Data
			(*episode).Episode = unpad(episodeName)
		} else if t.Data == "div" {
			for _, attr := range t.Attr {
				if attr.Key == "class" && strings.HasPrefix(attr.Val, "rls-link ") {
					(*episode).Torrents = append((*episode).Torrents, Torrent{
						Quality: attr.Val[strings.LastIndex(attr.Val, "-")+1:],
					})
					break
				}
			}
		} else if t.Data == "span" && episode != nil {
			for _, attr := range t.Attr {
				if attr.Key == "class" {
					z.Next()
					t = z.Token()
					if strings.HasSuffix(attr.Val, "hs-magnet-link") {
						for _, attr := range t.Attr {
							if attr.Key == "href" {
								(*episode).Torrents[len((*episode).Torrents)-1].Magnet = attr.Val
								break
							}
						}
					} else if !episode.XDCC && strings.HasSuffix(attr.Val, "hs-xdcc-link") {
						for _, attr := range t.Attr {
							if attr.Key == "href" {
								(*episode).XDCC = true
								break
							}
						}
					}
					break
				}
			}
		}
	}

	return
}

func GetDetails(link string) (details *Details, err error) {
	req, err := http.NewRequest(http.MethodGet, getAnimePage(link), nil)

	if err != nil {
		return
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	details = &Details{ID: -1, Link: link}

	z := html.NewTokenizer(res.Body)

	for {
		tt := z.Next()

		if tt == html.ErrorToken {
			break
		} else if tt != html.StartTagToken {
			continue
		}

		t := z.Token()
		if t.Data == "title" && len(details.Title) == 0 {
			z.Next()
			t = z.Token()
			(*details).Title = t.Data[:strings.LastIndex(t.Data, " –")]
		} else if t.Data == "div" {
			className := new(string)
			for _, v := range t.Attr {
				if v.Key == "class" {
					className = &v.Val
					break
				}
			}

			z.Next()
			z.Next()

			switch *className {
			case "series-desc":
				z.Next()
				var desc string
				desc, err = getDescription(z)

				if err != nil {
					return
				}

				(*details).Description = desc
			case "series-image":
				for _, v := range z.Token().Attr {
					if v.Key == "src" {
						(*details).Image = v.Val
						if !strings.HasPrefix(details.Image, "http") {
							(*details).Image = "https://horriblesubs.info" + details.Image
						}
						break
					}
				}
			}
		} else if t.Data == "script" && details.ID == -1 {
			z.Next()
			if t = z.Token(); strings.HasPrefix(t.Data, "var hs_showid = ") {
				var id int
				id, err = strconv.Atoi(t.Data[16 : len(t.Data)-1]) // 16 is the length of the prefix

				if err != nil {
					return
				}

				(*details).ID = id
			}
		}
	}

	if details.ID == -1 {
		err = ErrNoShowID
		return
	}

	semaphore := make(chan struct{}, 10)
	done := make(chan error, 10)
	ctx, cancel := context.WithCancel(context.Background())
	wg := new(sync.WaitGroup)
	lock := new(sync.Mutex)

	defer cancel()

ext:
	for page := 0; ; page++ {
		select {
		case err = <-done:
			wg.Wait()
			if err != nil {
				return
			}
			break ext
		default:
			semaphore <- struct{}{}
			wg.Add(1)
			break
		}

		go func(page int) {
			defer func() {
				<-semaphore
				wg.Done()
			}()

			episodes, err := getEpisodes(ctx, link, page, details.ID)

			if err != nil {
				done <- err
			} else if len((*episodes)) == 0 {
				done <- nil
				return
			}

			lock.Lock()
			defer lock.Unlock()

			(*details).Episodes = append((*details).Episodes, (*episodes)...)
		}(page)
	}

	sort.Slice(details.Episodes, func(i, j int) bool {
		firstEpisode, err := strconv.ParseFloat(details.Episodes[i].Episode, 10)

		if err != nil {
			return false
		}

		secondEpisode, err := strconv.ParseFloat(details.Episodes[j].Episode, 10)

		if err != nil {
			return false
		}

		return firstEpisode < secondEpisode
	})

	return
}
