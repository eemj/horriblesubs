package horriblesubs

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

var (
	ErrUnsupportedContentType = errors.New("unsupported content type")
	ErrMissingShow            = errors.New("missing show")

	xdccExp = regexp.MustCompile(`{b:"(.+)", n:(\d+), s:(\d+), f:"\[HorribleSubs\] (.+) - (.+) \[(.+)\].+"}`)
)

type XDCC struct {
	Bot      string
	Pack     int
	Filename string
	Quality  string
	Size     int
	Episode  string
}

func parseXdccContent(show, episode string, r io.Reader) (items *[]XDCC, err error) {
	items = new([]XDCC)

	body, err := ioutil.ReadAll(r)

	if err != nil {
		return
	}

	for _, text := range strings.Split(string(body), ";") {
		if len(text) <= 1 {
			continue
		}

		text = strings.TrimSpace(text)

		if !xdccExp.MatchString(text) {
			continue
		}

		matches := xdccExp.FindStringSubmatch(text)

		if len(strings.TrimSpace(episode)) == 0 {
			if strings.TrimSpace(matches[4]) != show {
				continue
			}
		} else {
			if unpad(matches[5]) != episode || strings.TrimSpace(matches[4]) != show {
				continue
			}
		}

		item := XDCC{
			Bot:      matches[1],
			Filename: matches[4],
			Quality:  matches[6],
			Episode:  unpad(matches[5]),
		}

		pack, err := strconv.Atoi(matches[2])

		if err != nil {
			return items, err
		}

		item.Pack = pack

		size, err := strconv.Atoi(matches[3])

		if err != nil {
			return items, err
		}

		item.Size = size

		(*items) = append((*items), item)
	}

	return
}

func GetXDCC(params ...string) (items *[]XDCC, err error) {
	items = new([]XDCC)

	var (
		show    string
		episode string
	)

	switch len(params) {
	case 0:
		err = ErrMissingShow
	case 1:
		show = params[0]
	case 2:
		show = params[0]
		episode = params[1]
	}

	uri := fmt.Sprintf(
		"http://xdcc.horriblesubs.info/search.php?t=%s",
		url.PathEscape(fmt.Sprintf("%s - %s", show, episode)),
	)

	req, err := http.NewRequest(http.MethodGet, uri, nil)

	if err != nil {
		return
	}

	setUserAgent(req)

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	contentType := res.Header.Get("Content-Type")

	if strings.HasPrefix(contentType, "text/plain") {
		items, err = parseXdccContent(show, episode, res.Body)
	} else if strings.HasPrefix(contentType, "text/html") {
		z := html.NewTokenizer(res.Body)

		for {
			tt := z.Next()

			if tt == html.ErrorToken {
				break
			} else if tt != html.StartTagToken {
				continue
			}

			t := z.Token()

			if t.Data == "pre" {
				z.Next()
				t = z.Token()
				items, err = parseXdccContent(
					show,
					episode,
					strings.NewReader(t.Data),
				)
				return
			}
		}
	} else {
		err = ErrUnsupportedContentType
	}

	return
}
