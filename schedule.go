package horriblesubs

import (
	"golang.org/x/net/html"
	"net/http"
	"strings"
	"time"
)

type Schedule struct {
	Weekdays []Weekday
}

type Weekday struct {
	Weekday  string
	Releases []Release
}

type Release struct {
	*Show
	Time time.Time
}

func GetSchedule() (schedule *Schedule, err error) {
	schedule = &Schedule{Weekdays: []Weekday{}}

	req, err := http.NewRequest(http.MethodGet, "http://horriblesubs.info/release-schedule", nil)
	setUserAgent(req)

	if err != nil {
		return
	}

	res, err := http.DefaultClient.Do(req)

	if err != nil {
		return
	}

	defer res.Body.Close()

	z := html.NewTokenizer(res.Body)

	previousWeekday := ""

	for {
		tt := z.Next()

		if tt == html.ErrorToken {
			break // should never come here, but it's here just in-case
		} else if tt != html.StartTagToken {
			continue
		}

		if t := z.Token(); t.Data == "h2" {
			weekdayFound := false
			for _, attr := range t.Attr {
				if attr.Key == "class" {
					z.Next()
					if attr.Val == "weekday" {
						previousWeekday = z.Token().Data
						weekdayFound = true
					} else if attr.Val == "weekday schedule-today" {
						previousWeekday = strings.Split(z.Token().Data, " ")[0]
						weekdayFound = true
					}
					break
				}
			}

			if weekdayFound {
				(*schedule).Weekdays = append((*schedule).Weekdays, Weekday{
					Weekday: previousWeekday,
				})
			}
		} else if t.Data == "td" {
			schedulePageShow := false
			releases := &((*schedule).Weekdays[len((*schedule).Weekdays)-1].Releases)
			for _, attr := range t.Attr {
				if attr.Key != "class" {
					continue
				}

				z.Next()
				t = z.Token()
				if attr.Val == "schedule-page-show" {
					schedulePageShow = true
				} else if attr.Val == "schedule-time" {
					parsed, _ := time.Parse("15:04", t.Data)
					(*releases)[len(*releases)-1].Time = parsed
				}

				break
			}

			if !schedulePageShow {
				continue
			}

			show := new(Show)
			for _, attr := range t.Attr {
				if attr.Key == "href" {
					(*show).Link = attr.Val[7:]
					break
				}
			}
			z.Next()
			(*show).Title = z.Token().Data

			(*releases) = append((*releases), Release{
				Show: show,
			})
		}
	}

	return
}
