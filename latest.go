package horriblesubs

import (
	"golang.org/x/net/html"
	"io"
	"net/http"
	"strings"
	"sync"
	"time"
)

type Latest struct {
	Title     string
	Link      string
	Episode   string
	Qualities []string
	Released  int64
}

func extractLatest(reader io.Reader) (latest []Latest, err error) {
	z := html.NewTokenizer(reader)

	for {
		tt := z.Next()

		if tt == html.ErrorToken {
			break
		} else if tt != html.StartTagToken {
			continue
		}

		if t := z.Token(); t.Data == "a" {
			for _, attr := range t.Attr {
				if attr.Key == "href" {
					latest = append(latest, Latest{
						Link: attr.Val[7:strings.LastIndex(attr.Val, "#")],
					})
					break
				}
			}
		} else if t.Data == "span" {
			for _, attr := range t.Attr {
				if attr.Key == "class" {
					curr := &latest[len(latest)-1]
					if attr.Val == "latest-releases-date" {
						z.Next()
						t = z.Token()

						now := time.Now()

						switch {
						case t.Data == "Today":
							curr.Released = now.Unix()
						case t.Data == "Yesterday":
							curr.Released = now.Add(-(24 * time.Hour)).Unix()
						default:
							parsed, err := time.Parse("Jan 2", t.Data[:len(t.Data)-2])

							if err != nil {
								return latest, err
							}

							curr.Released = parsed.Unix()
						}

						z.Next()
						z.Next()
						t = z.Token()
						curr.Title = t.Data[:len(t.Data)-3]
					} else if attr.Val == "badge" {
						z.Next()

						badge := z.Token().Data
						if badge == "SD" {
							badge = "480p"
						}

						curr.Qualities = append(curr.Qualities, badge)
					}

					break
				}
			}
		} else if t.Data == "strong" {
			z.Next()
			episode := z.Token().Data
			latest[len(latest)-1].Episode = unpad(episode)
		}
	}

	return
}

var uris = []string{
	"http://horriblesubs.info/api.php?method=getlatest",
	"http://horriblesubs.info/api.php?method=getlatest&nextid=0",
	"http://horriblesubs.info/api.php?method=getlatest&nextid=1",
	"http://horriblesubs.info/api.php?method=getlatest&nextid=2",
}

func GetLatest() (*[]Latest, error) {
	wg := new(sync.WaitGroup)
	errc := make(chan error)
	done := make(chan struct{})
	pages := make([][]Latest, len(uris))
	pagesLock := new(sync.Mutex)

	for offset, uri := range uris {
		wg.Add(1)

		go func(uri string, offset int) {
			defer wg.Done()

			req, err := http.NewRequest(http.MethodGet, uri, nil)

			setUserAgent(req)

			if err != nil {
				errc <- err
				return
			}

			res, err := http.DefaultClient.Do(req)

			if err != nil {
				errc <- err
				return
			}

			defer res.Body.Close()

			shows, err := extractLatest(res.Body)

			if err != nil {
				errc <- err
				return
			}

			pagesLock.Lock()
			defer pagesLock.Unlock()
			pages[offset] = shows
		}(uri, offset)
	}

	go func() {
		wg.Wait()
		done <- struct{}{}
	}()

	for {
		select {
		case err := <-errc:
			return nil, err
		case <-done:
			out := new([]Latest)
			for _, page := range pages {
				*out = append(*out, page...)
			}
			return out, nil
		}
	}
}
