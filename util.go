package horriblesubs

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

func setUserAgent(r *http.Request) {
	(*r).Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0")
}

func getAnimePage(show string) string {
	return fmt.Sprintf("http://horriblesubs.info/shows/%s", show)
}

func unpad(phrase string) (out string) {
	out = phrase
	if len(out) > 1 && out[0] == '0' {
		out = out[1:]
	}
	return
}

func getDescription(z *html.Tokenizer) (out string, err error) {
	desc := new(strings.Builder)
	anchor := false

check:
	for {
		tt := z.Next()
		t := z.Token()

		switch {
		case tt == html.TextToken:
			if !anchor {
				desc.WriteString(t.Data)
			}
		case tt == html.StartTagToken && t.Data == "a":
			anchor = true
			encoded := new(string)

			for _, attr := range t.Attr {
				if attr.Key == "data-cfemail" {
					encoded = &attr.Val
					break
				}
			}

			var decoded string
			decoded, err = decode(*encoded)

			if err != nil {
				return
			}

			desc.WriteString(decoded)
		case tt == html.EndTagToken && t.Data == "a":
			anchor = false
		case tt == html.EndTagToken && t.Data == "p":
			break check
		}
	}

	out = strings.TrimSpace(desc.String())
	return
}

func decode(phrase string) (out string, err error) {
	v, err := strconv.ParseInt(phrase[:2], 16, 16)

	if err != nil {
		return
	}

	b := new(strings.Builder)

	for i := 2; i < len(phrase)-1; i += 2 {
		p, _ := strconv.ParseInt(phrase[i:i+2], 16, 16)
		b.WriteString(string(p ^ v))
	}

	out = b.String()

	return
}
